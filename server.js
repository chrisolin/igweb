/*
	Expected environment variables:
	NODE_SESSION_SECRET: Given to express-session as the secret string
	NODE_ENV: development or other
	PORT
	REDIS_URL: such as 'redis://endpoint:6379'
 */

console.time('start application');

var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var permissions = require('./permissions');
var routes = require('./routes');

var app = express();
var engine = require('ejs-locals');

app.set('siteTitle', 'Industry Games');

// view engine setup
app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon());
app.use(logger(app.get('env') === 'development' ? 'dev' : 'default'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

// local variables for development, redis for production
if (app.get('env') === 'development') {
	app.use(session({secret: process.env.NODE_SESSION_SECRET || 'my secret string'}));
} else {
	if (!process.env.REDIS_URL) {
		throw new Error('REDIS_URL must be defined');
		process.close();
	}
	console.log('connecting sessions to redis store');
	var RedisStore = require('connect-redis')(session);
	var redisOptions = {
		url: process.env.REDIS_URL
	};
	app.use(session({store: new RedisStore(redisOptions), secret: process.env.NODE_SESSION_SECRET || 'my secret string'}));
}

app.use(express.static(path.join(__dirname, 'public')));

app.use(permissions);
app.use(routes);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
	res.status(404);
    res.render('404');
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('500');
});

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
	console.log('Express server listening on port ' + server.address().port);
	console.timeEnd('start application');
});