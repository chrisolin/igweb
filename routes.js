var express = require('express');
var home = require('./controllers/home');
var app = module.exports = express();

app.get('/', home.home);
app.get('/contact', home.contact);
app.get('/about', home.about);