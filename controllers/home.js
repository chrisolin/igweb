/**
 * Created by chrisolin on 5/14/2014.
 */

module.exports = {
	home: function (req, res) {
		res.render('home/home', {pageTitle: 'Home'});
	},
	contact: function (req, res) {
		res.render('home/contact', {pageTitle: 'Contact'});
	},
	about: function (req, res) {
		res.render('home/about', {pageTitle: 'About'});
	}
}
